# SGSuperDeals

[![pipeline status](https://gitlab.com/luazhizhan/sg-super-deals/badges/master/pipeline.svg)](https://gitlab.com/luazhizhan/sg-super-deals/-/commits/master)

This is a [TypeScript](https://www.typescriptlang.org/) [React](https://reactjs.org/) Single Page Application(SPA) project. This website provide information about the latest deals and promotions in Singapore. The website is deployed to [netlify](https://www.netlify.com/).

Check out the [website here](https://sgsuperdeals.netlify.app/).

[[_TOC_]]

## Get Started

1. Install [Node.js](https://nodejs.org/)

2. Install [Yarn package manager](https://classic.yarnpkg.com/en/docs/install)

3. Install packages for this project by running this command on your terminal `yarn`

4. Run `yarn start` to start a local instance of the project

## Folder structure

```bash
│   .env # environment variables
│   .eslintrc.js # ESLint configs
│   .gitignore
│   .gitlab-ci.yml
│   .prettierrc.js # Prettier configs
│   netlify.toml # Netlify configs
│   package.json
│   README.md
│   tsconfig.json # TypeScript configs
│   yarn.lock
├───.vscode # VSCode workspace settings
├───public
│   └───images
└───src
    │   App.tsx
    │   index.tsx
    │   react-app-env.d.ts
    │   Routes.ts  # List of web page routes
    │   setupTests.ts
    ├───assets
    ├───components # Reusable React components
    ├───configs
    ├───helpers # Reusable code snippets
    ├───interfaces
    ├───layouts  # Layout design for the website
    ├───pages
    ├───redux
    └───__test__
```

## Technology stack

Below is a list of technology used by this project.

1. React TypeScript

2. [Firebase](https://firebase.google.com/) (Firestore, Firebase Storage)

3. Netlify (Website hosting)

## Core modules

Below is a list of core modules used by this project.

1. React

2. TypeScript

3. Firebase

4. [Material UI](https://material-ui.com/) - Main theme

5. [React Router](https://reacttraining.com/react-router/web/guides/quick-start)

6. [Redux](https://redux.js.org/) - State management

7. [Redux Saga](https://redux-saga.js.org/) - State management using [Generators](https://github.com/gajus/gajus.com-blog/blob/master/posts/the-definitive-guide-to-the-javascript-generators/index.md)

8. [Redux Saga Firbease](https://redux-saga-firebase.js.org/)

9. [ESlint](https://eslint.org/) - Linter

10. [Prettier](https://prettier.io/) - Code formatter

## Environment Variables

There are three values inside the environment variables `.env` file. For React project, the [environment variables](https://create-react-app.dev/docs/adding-custom-environment-variables/) must start with `REACT_APP_` in order to access it in the project.

```text
REACT_APP_META_DESC=<VALUE>
REACT_APP_META_TITLE=<VALUE>
REACT_APP_GOOGLE_MAP_KEY=<VALUE>
```

## GitLab CI/CD

[GitLab CI/CD](https://docs.gitlab.com/ee/ci/) pipeline has been configured for this project. The pipeline will only triggered when there is a push to the master branch.

There are 1 [stage](https://docs.gitlab.com/ee/ci/yaml/#stages)(deploy) and 1 [job](https://docs.gitlab.com/ee/ci/yaml/#introduction)(netlify) for this pipeline. The job will check formatting, linting of the code and create a production build to be deployed to [netlify](https://www.netlify.com/).

Learn more about GitLab CI/CD by looking at [configuration reference](https://docs.gitlab.com/ee/ci/yaml/README.html)

### Pipeline Variables

GitLab CI/CD provides a [storage](https://gitlab.com/help/ci/variables/README#variables) for secrets, API keys and password to be consume by the pipeline. It can be found [here](https://gitlab.com/luazhizhan/sg-super-deals/-/settings/ci_cd).
