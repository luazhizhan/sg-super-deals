/* eslint-disable @typescript-eslint/no-explicit-any */
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { types, latestPromoDataSuccess, errorInLatestPromo } from './actions';
import Rsf from '../../helpers/Rsf';
import Promotion from '../../interfaces/data/Promotion';
import { docToPromo } from '../../helpers/Firestore';

function* fetchLatestPromoDataSaga() {
  try {
    const promoCol: any = Rsf.app
      .firestore()
      .collection('promotions')
      .orderBy('editedDate', 'desc')
      .limit(5);
    const snapshot = yield call(Rsf.firestore.getCollection, promoCol);
    const promoDocs: Promotion[] = snapshot.docs.map((doc: any) => docToPromo(doc));
    yield put(
      latestPromoDataSuccess({
        promo: promoDocs,
      }),
    );
  } catch (error) {
    yield put(errorInLatestPromo({ error: 'An error has occured, please try again later' }));
  }
}

export default function* homeRootSaga() {
  yield all([takeEvery(types.LATEST_PROMOTION.LATEST_PROMO_REQUEST, fetchLatestPromoDataSaga)]);
}
