import { types } from './actions';
import HomeState from '../../interfaces/redux/Home';
import Promotion from '../../interfaces/data/Promotion';

const initialState: HomeState = {
  loading: false,
  isError: false,
  error: '',
  latestPromo: [],
};

export default function homeReducer(
  state: HomeState = initialState,
  action: {
    type: string;
    data: {
      error: string;
      promo: Promotion[];
    };
  },
) {
  switch (action.type) {
    case types.LATEST_PROMOTION.LATEST_PROMO_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.LATEST_PROMOTION.LATEST_PROMO_SUCCESS:
      return {
        ...initialState,
        latestPromo: action.data.promo,
      };
    case types.HOME_ERROR:
      return {
        ...state,
        loading: false,
        isError: true,
        error: action.data.error,
      };
    default:
      return state;
  }
}
