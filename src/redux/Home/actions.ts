import Promotion from '../../interfaces/data/Promotion';

export const types = {
  LATEST_PROMOTION: {
    LATEST_PROMO_REQUEST: 'LATEST_PROMO_REQUEST',
    LATEST_PROMO_SUCCESS: 'LATEST_PROMO_SUCCESS',
  },
  HOME_ERROR: 'HOME_ERROR',
};

export const latestPromoDataRequest = () => ({
  type: types.LATEST_PROMOTION.LATEST_PROMO_REQUEST,
});

export const latestPromoDataSuccess = (data: { promo: Promotion[] }) => ({
  type: types.LATEST_PROMOTION.LATEST_PROMO_SUCCESS,
  data,
});

export const errorInLatestPromo = (data: { error: string }) => ({
  type: types.HOME_ERROR,
  data,
});
