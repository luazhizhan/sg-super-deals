export const types = {
  SNACKBAR: {
    SHOW_SKBAR: 'SHOW_SKBAR',
    HIDE_SKBAR: 'HIDE_SKBAR',
  },
  DRAWER: {
    SHOW_DRWER: 'SHOW_DRWER',
    HIDE_DRWER: 'HIDE_DRWER',
    SEL_UPDATE: 'SEL_UPDATE',
  },
};

export const showSnackbar = (data: {
  msg: string;
  variant: 'success' | 'warning' | 'error' | 'info' | 'copy';
}) => ({
  type: types.SNACKBAR.SHOW_SKBAR,
  data,
});

export const hideSnackbar = () => ({
  type: types.SNACKBAR.HIDE_SKBAR,
});

export const drawerOpen = () => ({
  type: types.DRAWER.SHOW_DRWER,
});

export const drawerClose = () => ({
  type: types.DRAWER.HIDE_DRWER,
});

export const drawerItemSelUpdate = (data: { sel: string }) => ({
  type: types.DRAWER.SEL_UPDATE,
  data,
});
