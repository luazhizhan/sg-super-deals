import { types } from './actions';
import RootState from '../../interfaces/redux/Root';

const initialState: RootState = {
  drawer: {
    open: false,
    sel: 'home',
  },
  snackbar: {
    open: false,
    msg: '',
    variant: 'info',
  },
};

export default function rootReducer(
  state: RootState = initialState,
  action: {
    type: string;
    data: {
      msg: string;
      variant: 'success' | 'warning' | 'error' | 'info' | 'copy';
      sel: string;
    };
  },
) {
  switch (action.type) {
    case types.SNACKBAR.SHOW_SKBAR:
      return {
        ...state,
        snackbar: {
          open: true,
          msg: action.data.msg,
          variant: action.data.variant,
        },
      };
    case types.SNACKBAR.HIDE_SKBAR:
      return {
        ...state,
        snackbar: {
          ...state.snackbar,
          open: false,
        },
      };
    case types.DRAWER.SHOW_DRWER:
      return {
        ...state,
        drawer: {
          ...state.drawer,
          open: true,
        },
      };
    case types.DRAWER.HIDE_DRWER:
      return {
        ...state,
        drawer: {
          ...state.drawer,
          open: false,
        },
      };
    case types.DRAWER.SEL_UPDATE:
      return {
        ...state,
        drawer: {
          open: false,
          sel: action.data.sel,
        },
      };
    default:
      return state;
  }
}
