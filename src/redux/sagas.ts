import { all } from 'redux-saga/effects';
import homeRootSaga from './Home/sagas';
import catalogueRootSaga from './Catalogue/sagas';
import detailsRootSaga from './Details/sagas';

export default function* rootSaga() {
  yield all([homeRootSaga(), catalogueRootSaga(), detailsRootSaga()]);
}
