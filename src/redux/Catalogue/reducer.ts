import { types } from './actions';
import CatalogueState from '../../interfaces/redux/Catalogue';
import Promotion from '../../interfaces/data/Promotion';

const initialState: CatalogueState = {
  loading: false,
  isError: false,
  error: '',
  variants: { col: 'contained', list: 'outlined' },
  promo: {
    selected: undefined,
    data: [],
    viewData: [],
  },
  keywords: {
    selected: '',
    data: [],
  },
  categories: {
    selected: '',
    data: [],
  },
  search: '',
  sortBy: 'descDate',
};

function setFilterOption(input: string, stateValue: string) {
  // Unselect filter the same filter has been selected
  return input === stateValue ? '' : input;
}

export default function catalogueReducer(
  state: CatalogueState = initialState,
  action: {
    type: string;
    data: {
      variants: {};
      error: string;
      promo: Promotion[];
      categories: [];
      keywords: [];
      sortBy: string;
      category: string;
      keyword: string;
      search: string;
    };
  },
) {
  switch (action.type) {
    case types.SET_VIEW_VARIANTS:
      return {
        ...state,
        variants: action.data.variants,
      };
    case types.ALL_PROMOTION.ALL_PROMO_REQUEST:
      return {
        ...state,
        variants: state.variants,
        loading: true,
      };
    case types.ALL_PROMOTION.ALL_PROMO_SUCCESS:
      return {
        ...state,
        loading: false,
        promo: {
          selected: undefined,
          data: action.data.promo,
          viewData: action.data.promo,
        },
        keywords: {
          ...state.keywords,
          data: action.data.keywords,
        },
        categories: {
          selected: '',
          data: action.data.categories,
        },
      };
    case types.SORT_REQUEST:
      return {
        ...state,
        sortBy: action.data.sortBy,
      };
    case types.CATEGORY_REQUEST:
      return {
        ...state,
        categories: {
          ...state.categories,
          selected: setFilterOption(action.data.category, state.categories.selected),
        },
        keywords: {
          ...state.keywords,
          selected: '',
        },
        search: '',
      };
    case types.KEYWORD_REQUEST:
      return {
        ...state,
        categories: {
          ...state.categories,
          selected: '',
        },
        keywords: {
          ...state.keywords,
          selected: setFilterOption(action.data.keyword, state.keywords.selected),
        },
        search: '',
      };
    case types.SEARCH_REQUEST:
      return {
        ...state,
        categories: {
          ...state.categories,
          selected: '',
        },
        keywords: {
          ...state.keywords,
          selected: '',
        },
        search: action.data.search.trim(),
      };
    case types.SEARCH_CLEAR:
      return {
        ...state,
        search: '',
      };
    case types.UPDATE_VIEW_PROMO:
      return {
        ...state,
        promo: {
          ...state.promo,
          viewData: action.data.promo,
        },
      };
    case types.CATALOGUE_ERROR:
      return {
        ...state,
        loading: false,
        isError: true,
        error: action.data.error,
      };
    default:
      return state;
  }
}
