/* eslint-disable @typescript-eslint/no-explicit-any */
import { all, call, put, takeEvery, select, SelectEffect } from 'redux-saga/effects';
import { types, allPromoDataSuccess, errorInPromo, updateViewPromo } from './actions';
import { docToPromo } from '../../helpers/Firestore';
import Rsf from '../../helpers/Rsf';
import { concatUniqueValFromArrObj } from '../../helpers/Array';
import CatalogueState from '../../interfaces/redux/Catalogue';
import Promotion from '../../interfaces/data/Promotion';

function getCategoriesAndKeywords(data: Promotion[]) {
  const categories: string[] = [];
  let keywords: string[] = [];
  data.forEach((promo: Promotion) => {
    if (!categories.some((category) => category === promo.category)) {
      categories.push(promo.category);
    }
    keywords = concatUniqueValFromArrObj(keywords, promo.keywords);
  });
  return { categories, keywords };
}

function sortPromoData(sortBy: string, promoData: Promotion[]): Promotion[] {
  if (sortBy === 'descDate') {
    return promoData.sort(
      (a, b) => new Date(b.editedDate).getTime() - new Date(a.editedDate).getTime(),
    );
  }
  return promoData.sort(
    (a, b) => new Date(a.editedDate).getTime() - new Date(b.editedDate).getTime(),
  );
}

function* getCatalogueState(): Generator<SelectEffect, CatalogueState, CatalogueState> {
  const getCatalogue = (state: { Catalogue: CatalogueState }) => state.Catalogue;
  const catalogueState: CatalogueState = yield select(getCatalogue);
  return catalogueState;
}

function* categorisedPromoDataSaga() {
  const catalogueState: CatalogueState = yield getCatalogueState();
  let viewPromoData: Promotion[] = sortPromoData(catalogueState.sortBy, catalogueState.promo.data);
  if (catalogueState.categories.selected) {
    viewPromoData = viewPromoData.filter((a) => a.category === catalogueState.categories.selected);
  }
  yield put(updateViewPromo({ promo: viewPromoData }));
}

function* keywordPromoDataSaga() {
  const catalogueState: CatalogueState = yield getCatalogueState();
  let viewPromoData: Promotion[] = sortPromoData(catalogueState.sortBy, catalogueState.promo.data);
  if (catalogueState.keywords.selected) {
    viewPromoData = viewPromoData.filter((a) =>
      a.keywords.includes(catalogueState.keywords.selected),
    );
  }
  yield put(updateViewPromo({ promo: viewPromoData }));
}

function* sortPromoDataSaga() {
  const catalogueState: CatalogueState = yield getCatalogueState();
  let viewPromoData: Promotion[] = sortPromoData(catalogueState.sortBy, catalogueState.promo.data);
  if (catalogueState.categories.selected !== '') {
    viewPromoData = viewPromoData.filter((a) => a.category === catalogueState.categories.selected);
  } else if (catalogueState.keywords.selected !== '') {
    viewPromoData = viewPromoData.filter((a) =>
      a.keywords.includes(catalogueState.keywords.selected),
    );
  }
  yield put(updateViewPromo({ promo: viewPromoData }));
}

function* searchPromoDataSaga() {
  const catalogueState: CatalogueState = yield getCatalogueState();
  let viewPromoData: Promotion[] = sortPromoData(catalogueState.sortBy, catalogueState.promo.data);
  if (catalogueState.search) {
    viewPromoData = viewPromoData.filter((promo: Promotion) =>
      promo.title.toLowerCase().includes(catalogueState.search),
    );
  }
  yield put(updateViewPromo({ promo: viewPromoData }));
}

function* fetchPromoDataSaga() {
  try {
    const promoCol: any = Rsf.app
      .firestore()
      .collection('promotions')
      .orderBy('editedDate', 'desc');
    const snapshot = yield call(Rsf.firestore.getCollection, promoCol);
    const promoDocs: Promotion[] = snapshot.docs.map((doc: any) => docToPromo(doc));
    const categoryKeyWordObj = getCategoriesAndKeywords(promoDocs);
    const { categories, keywords } = categoryKeyWordObj;
    yield put(
      allPromoDataSuccess({
        promo: promoDocs,
        categories,
        keywords,
      }),
    );

    // Update view data if keyword is selected
    const catalogueState: CatalogueState = yield getCatalogueState();
    if (catalogueState.keywords.selected) yield keywordPromoDataSaga();
  } catch (error) {
    yield put(errorInPromo({ error: 'An error has occured, please try again later' }));
  }
}

function* clearSearchPromoDataSaga() {
  const catalogueState: CatalogueState = yield getCatalogueState();
  const viewPromoData: Promotion[] = sortPromoData(
    catalogueState.sortBy,
    catalogueState.promo.data,
  );
  yield put(updateViewPromo({ promo: viewPromoData }));
}

export default function* catalogueRootSaga() {
  yield all([takeEvery(types.ALL_PROMOTION.ALL_PROMO_REQUEST, fetchPromoDataSaga)]);
  yield all([takeEvery(types.CATEGORY_REQUEST, categorisedPromoDataSaga)]);
  yield all([takeEvery(types.KEYWORD_REQUEST, keywordPromoDataSaga)]);
  yield all([takeEvery(types.SORT_REQUEST, sortPromoDataSaga)]);
  yield all([takeEvery(types.SEARCH_REQUEST, searchPromoDataSaga)]);
  yield all([takeEvery(types.SEARCH_CLEAR, clearSearchPromoDataSaga)]);
}
