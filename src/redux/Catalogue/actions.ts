import Promotion from '../../interfaces/data/Promotion';
import ViewTypesBtnGrp from '../../interfaces/pages/ViewTypesBtnGrp';

export const types = {
  SET_VIEW_VARIANTS: 'SET_VIEW_VARIENTS',
  ALL_PROMOTION: {
    ALL_PROMO_REQUEST: 'ALL_PROMO_REQUEST',
    ALL_PROMO_SUCCESS: 'ALL_PROMO_SUCCESS',
  },
  CATEGORY_REQUEST: 'CATEGORY_REQUEST',
  KEYWORD_REQUEST: 'KEYWORD_REQUEST',
  SORT_REQUEST: 'SORT_REQUEST',
  SEARCH_REQUEST: 'SEARCH_REQUEST',
  SEARCH_CLEAR: 'SEARCH_CLEAR',
  UPDATE_VIEW_PROMO: 'UPDATE_VIEW_PROMO',
  CATALOGUE_ERROR: 'CATALOGUE_ERROR',
};

export const setViewVariants = (data: { variants: ViewTypesBtnGrp }) => ({
  type: types.SET_VIEW_VARIANTS,
  data,
});

export const allPromoDataRequest = () => ({
  type: types.ALL_PROMOTION.ALL_PROMO_REQUEST,
});

export const allPromoDataSuccess = (data: {
  promo: Promotion[];
  categories: string[];
  keywords: string[];
}) => ({
  type: types.ALL_PROMOTION.ALL_PROMO_SUCCESS,
  data,
});

export const categoryRequest = (data: { category: string }) => ({
  type: types.CATEGORY_REQUEST,
  data,
});

export const keywordRequest = (data: { keyword: string }) => ({
  type: types.KEYWORD_REQUEST,
  data,
});

export const sortRequest = (data: { sortBy: string }) => ({
  type: types.SORT_REQUEST,
  data,
});

export const searchRequest = (data: { search: string }) => ({
  type: types.SEARCH_REQUEST,
  data,
});

export const clearSearch = () => ({
  type: types.SEARCH_CLEAR,
});

export const updateViewPromo = (data: { promo: Promotion[] }) => ({
  type: types.UPDATE_VIEW_PROMO,
  data,
});

export const errorInPromo = (data: { error: string }) => ({
  type: types.CATALOGUE_ERROR,
  data,
});
