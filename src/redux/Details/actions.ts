import Promotion from '../../interfaces/data/Promotion';

export const types = {
  PROMOTION_ID: {
    PROMO_ID_REQUEST: 'PROMO_ID_REQUEST',
    PROMO_ID_SUCCESS: 'PROMO_ID_SUCCESS',
    PROMO_ID_NOT_FOUND: 'PROMO_ID_NOT_FOUND',
  },
  SEL_LOC_UPDATE: 'SEL_LOC_UPDATE',
  DETAILS_ERROR: 'DETAILS_ERROR',
};

export const getPromoByIdRequest = (data: { promoId: string }) => ({
  type: types.PROMOTION_ID.PROMO_ID_REQUEST,
  data,
});

export const getPromoByIdSuccess = (data: { promo: Promotion; selLoc: string }) => ({
  type: types.PROMOTION_ID.PROMO_ID_SUCCESS,
  data,
});

export const getPromoIdNotFound = () => ({
  type: types.PROMOTION_ID.PROMO_ID_NOT_FOUND,
});

export const selLocationUpdate = (data: { selLoc: string }) => ({
  type: types.SEL_LOC_UPDATE,
  data,
});

export const errorInDetails = (data: { error: string }) => ({
  type: types.DETAILS_ERROR,
  data,
});
