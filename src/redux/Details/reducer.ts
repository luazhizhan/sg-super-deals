import { types } from './actions';
import DetailsState from '../../interfaces/redux/Details';
import Promotion from '../../interfaces/data/Promotion';

const initialState: DetailsState = {
  loading: false,
  isError: false,
  error: '',
  promoId: '',
  selLoc: '',
  promo: undefined,
  exists: true,
};

export default function homeReducer(
  state: DetailsState = initialState,
  action: {
    type: string;
    data: {
      error: string;
      promoId: string;
      selLoc: string;
      promo: Promotion[];
    };
  },
) {
  switch (action.type) {
    case types.PROMOTION_ID.PROMO_ID_REQUEST:
      return {
        ...state,
        loading: true,
        promoId: action.data.promoId,
      };
    case types.PROMOTION_ID.PROMO_ID_SUCCESS:
      return {
        ...initialState,
        promo: action.data.promo,
        selLoc: action.data.selLoc,
      };
    case types.PROMOTION_ID.PROMO_ID_NOT_FOUND:
      return {
        ...initialState,
        exists: false,
      };
    case types.SEL_LOC_UPDATE:
      return {
        ...state,
        selLoc: action.data.selLoc,
      };
    case types.DETAILS_ERROR:
      return {
        ...state,
        loading: false,
        isError: true,
        error: action.data.error,
      };
    default:
      return state;
  }
}
