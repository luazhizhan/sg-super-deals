import { all, call, put, takeEvery, select } from 'redux-saga/effects';
import { types, getPromoByIdSuccess, getPromoIdNotFound, errorInDetails } from './actions';
import Rsf from '../../helpers/Rsf';
import Promotion from '../../interfaces/data/Promotion';
import DetailsState from '../../interfaces/redux/Details';
import { docToPromo } from '../../helpers/Firestore';

function* fetchPromoByIdSaga() {
  try {
    const getDetails = (state: { Details: DetailsState }) => state.Details;
    const detailsState: DetailsState = yield select(getDetails);
    const snapshot = yield call(Rsf.firestore.getDocument, `promotions/${detailsState.promoId}`);
    if (snapshot.exists) {
      const promo: Promotion = docToPromo(snapshot);
      yield put(
        getPromoByIdSuccess({
          promo,
          selLoc: promo.locations ? promo.locations[0] : '',
        }),
      );
    } else {
      yield put(getPromoIdNotFound());
    }
  } catch (error) {
    yield put(errorInDetails({ error: 'An error has occured, please try again later' }));
  }
}

export default function* detailsRootSaga() {
  yield all([takeEvery(types.PROMOTION_ID.PROMO_ID_REQUEST, fetchPromoByIdSaga)]);
}
