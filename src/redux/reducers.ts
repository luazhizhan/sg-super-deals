import Root from './Root/reducer';
import Home from './Home/reducer';
import Catalogue from './Catalogue/reducer';
import Details from './Details/reducer';

export default {
  Root,
  Home,
  Catalogue,
  Details,
};
