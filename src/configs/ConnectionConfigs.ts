const SourceConfigs = {
  jsonbin: 'https://api.jsonbin.io/b/',
};

const { jsonbin } = SourceConfigs;

const ConnectionConfig = {
  promotions: `${jsonbin}5e195b935df6407208335ca8/latest`,
};

export default ConnectionConfig;
