// eslint-disable-next-line import/prefer-default-export
export const GooleMapEmbedURL = (location: string) =>
  `https://www.google.com/maps/embed/v1/place?q=${encodeURIComponent(location)}&key=${
    process.env.REACT_APP_GOOGLE_MAP_KEY
  }`;
