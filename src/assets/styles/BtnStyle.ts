import { makeStyles } from '@material-ui/core/styles';

const btnUseStyles = makeStyles({
  noWrapbtn: {
    wordBreak: 'keep-all',
    whiteSpace: 'nowrap',
  },
});

export default btnUseStyles;
