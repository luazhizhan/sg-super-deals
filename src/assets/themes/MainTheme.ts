import { createMuiTheme } from '@material-ui/core/styles';
import { pink, deepPurple } from '@material-ui/core/colors';

const MainTheme = createMuiTheme({
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '*': {
          boxSizing: 'border-box',
          scrollBehavior: 'smooth',
        },
        body: {
          margin: '0 auto',
        },
      },
    },
  },
  palette: {
    primary: pink,
    secondary: deepPurple,
    text: {
      secondary: '#fafafa',
    },
  },
  typography: {
    fontFamily: 'roboto, sans-serif',
  },
});

export default MainTheme;
