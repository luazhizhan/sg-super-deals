import firebase from 'firebase/app';
import '@firebase/firestore';
import ReduxSagaFirebase from 'redux-saga-firebase';
import FirebaseConfigs from '../configs/FirebaseConfigs';

const FirebaseApp = firebase.initializeApp(FirebaseConfigs);

const Rsf = new ReduxSagaFirebase(FirebaseApp);

export default Rsf;
