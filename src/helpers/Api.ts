import axios from 'axios';

// eslint-disable-next-line import/prefer-default-export
export const fetchApi = async (apiOptions: { url: string; secretKey?: string }) => {
  const { url, secretKey } = apiOptions;
  try {
    const response = await axios.request({
      url,
      timeout: 30000,
      method: 'GET',
      headers: { 'Content-Type': 'application/json', 'secret-key': secretKey },
    });
    return { data: response.data };
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);
    return { error };
  }
};
