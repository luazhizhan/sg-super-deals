/* eslint-disable import/prefer-default-export */
import Promotion from '../interfaces/data/Promotion';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function docToPromo(doc: any): Promotion {
  const data = doc.data();
  return {
    ...data,
    promotionID: doc.id,
    dueDate: data.dueDate ? new Date(data.dueDate.seconds * 1000) : undefined,
    creationDate: new Date(data.creationDate.seconds * 1000),
    editedDate: new Date(data.editedDate.seconds * 1000),
  };
}
