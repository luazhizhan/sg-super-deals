// eslint-disable-next-line import/prefer-default-export
export function scrollToAnchor(id: string) {
  const elem: HTMLElement | null = document.getElementById(id);
  if (elem) {
    elem.scrollIntoView();
  }
}
