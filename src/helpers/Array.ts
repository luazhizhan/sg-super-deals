// eslint-disable-next-line import/prefer-default-export
export function concatUniqueValFromArrObj(a: string[], b: string[]) {
  return Array.from(new Set(a.concat(b)));
}
