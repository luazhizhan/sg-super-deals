import React, { useEffect, useCallback } from 'react';
import { push } from 'connected-react-router';
import { useSelector, useDispatch } from 'react-redux';
import { showSnackbar, drawerItemSelUpdate } from '../redux/Root/actions';
import { latestPromoDataRequest } from '../redux/Home/actions';
import HomeState from '../interfaces/redux/Home';
import PromoGallery from './Home/PromoGallary/PromoGallery';
import Intro from './Home/Intro/Intro';
import About from './Home/About/About';
import DealPromo from './Home/DealPromo/DealPromo';

function Home(): JSX.Element {
  const dispatch = useDispatch();
  // const history = useHistory();
  const homeState = useSelector((state: { Home: HomeState }) => state.Home);
  const { latestPromo, loading, isError, error } = homeState;

  const gallaryClick = (promoId: string) => {
    dispatch(push(`/details/${promoId}`));
  };

  const fetchData = useCallback(async () => {
    dispatch(drawerItemSelUpdate({ sel: 'home' }));
    dispatch(latestPromoDataRequest());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      dispatch(showSnackbar({ msg: error, variant: 'error' }));
    } else {
      fetchData();
    }
  }, [dispatch, error, fetchData, isError]);

  return (
    <>
      <Intro />
      <DealPromo />
      {latestPromo ? (
        <PromoGallery data={latestPromo} gallaryClick={gallaryClick} loading={loading} />
      ) : null}
      <About />
    </>
  );
}

export default Home;
