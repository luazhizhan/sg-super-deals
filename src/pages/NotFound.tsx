import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import notFoundImg from '../assets/images/404.svg';

const useStyles = makeStyles((theme) => ({
  div: {
    margin: '0 auto',
    width: '60%',
    [theme.breakpoints.down('xs')]: {
      width: '98%',
    },
  },
  card: {
    margin: '1.5em auto',
  },
  image: {
    height: 250,
    [theme.breakpoints.down('xs')]: {
      height: 140,
    },
  },
}));

function NotFound() {
  const classes = useStyles();
  return (
    <div className={classes.div}>
      <div style={{ textAlign: 'center' }}>
        <img src={notFoundImg} alt="404" className={classes.image} />
      </div>
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="h6" gutterBottom>
            Page not found.
          </Typography>
          <Typography variant="body1" gutterBottom>
            The page that you are looking for cannot be found.
          </Typography>
        </CardContent>
      </Card>
      <Button href="/" color="primary" variant="contained">
        Back
      </Button>
    </div>
  );
}

export default NotFound;
