/* eslint-disable no-nested-ternary */
import React, { useCallback, useEffect } from 'react';
import { push } from 'connected-react-router';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { getPromoByIdRequest, selLocationUpdate } from '../redux/Details/actions';
import { keywordRequest } from '../redux/Catalogue/actions';
import { showSnackbar } from '../redux/Root/actions';
import DetailsState from '../interfaces/redux/Details';

import DescCard from './Details/DescCard/DescCard';
import KeywordsCard from './Details/KeywordsCard/KeywordsCard';
import SrcCard from './Details/SrcCard/SrcCard';
import MapCard from './Details/MapCard/MapCard';
import LoadingPanel from './Details/LoadingPanel/LoadingPanel';
import NotFoundPanel from './Details/NotFoundPanel/NotFoundPanel';
import useStyles from './Details/Style';

function Details(): JSX.Element {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const params: any = useParams();
  const dispatch = useDispatch();
  const classes = useStyles();
  const detailsState = useSelector((state: { Details: DetailsState }) => state.Details);
  const { promo, selLoc, loading, isError, error, exists } = detailsState;

  const keywordClick = (keyword: string) => {
    dispatch(keywordRequest({ keyword }));
    dispatch(push(`/catalogue`));
  };

  const locationClick = (location: string) => {
    dispatch(selLocationUpdate({ selLoc: location }));
  };

  const handleCopyCodeClick = () => {
    dispatch(showSnackbar({ msg: 'Copy code to clipboard successfully', variant: 'copy' }));
  };

  const fetchData = useCallback(async () => {
    dispatch(getPromoByIdRequest({ promoId: params.id }));
  }, [dispatch, params.id]);

  useEffect(() => {
    window.scrollTo(0, 0);
    if (isError) {
      dispatch(showSnackbar({ msg: error, variant: 'error' }));
    } else {
      fetchData();
    }
  }, [dispatch, error, fetchData, isError]);

  return (
    <>
      {!exists ? (
        <NotFoundPanel />
      ) : promo && !loading ? (
        <div className={classes.div}>
          <img src={promo.imageUrl} alt={promo.title} className={classes.img} />
          <DescCard promo={promo} handleCopyCodeClick={handleCopyCodeClick} />
          <MapCard promo={promo} selLoc={selLoc} locationClick={locationClick} />
          <KeywordsCard promo={promo} keywordClick={keywordClick} />
          <SrcCard promo={promo} />
          <Button href="/" color="primary" variant="contained" className={classes.btn}>
            Back
          </Button>
        </div>
      ) : (
        <LoadingPanel />
      )}
    </>
  );
}

export default Details;
