import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  card: {
    display: 'flex',
    flexWrap: 'wrap',
    minHeight: '18em',
    margin: '2em 0',
  },
  cardMedia: {
    minHeight: '18em',
    backgroundSize: 'contain !important',
    width: '28%',
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  cardBody: {
    width: '68%',
    marginLeft: '2em',
    [theme.breakpoints.down('xs')]: {
      width: '100%',
      marginLeft: 0,
    },
  },
  cardContent: {
    minHeight: '14em',
  },
}));

export default useStyles;
