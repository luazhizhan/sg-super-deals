import React from 'react';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import useStyles from './Style';
import PromoDesc from '../../../components/PromoDesc';
import PromoCardActions from '../../../components/PromoCardActions';
import Promotion from '../../../interfaces/data/Promotion';

function PromoList(props: { data: Promotion[] }): JSX.Element {
  const { data } = props;
  const classes = useStyles();
  return (
    <>
      {data.map((promo) => (
        <Card key={promo.promotionID} className={classes.card}>
          <CardMedia image={promo.imageUrl} className={classes.cardMedia} />
          <div className={classes.cardBody}>
            <CardContent className={classes.cardContent}>
              <Typography variant="h6" gutterBottom>
                {promo.title}
              </Typography>
              <PromoDesc promo={promo} />
            </CardContent>
            <PromoCardActions promo={promo} />
          </div>
        </Card>
      ))}
    </>
  );
}

export default PromoList;
