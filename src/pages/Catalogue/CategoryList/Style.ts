import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  list: {
    backgroundColor: theme.palette.background.paper,
    margin: '0.5em 0',
    boxShadow: theme.shadows[1],
    overflowY: 'auto',
  },
  avatar: {
    backgroundColor: `${theme.palette.primary.main} !important`,
  },
}));

export default useStyles;
