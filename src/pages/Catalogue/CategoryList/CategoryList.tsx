import React from 'react';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import LocalActivitySharpIcon from '@material-ui/icons/LocalActivitySharp';
import Skeleton from './Skeleton';
import useStyles from './Style';

function CategoryList(props: {
  categoryList: string[];
  loading: boolean;
  selCategory: string;
  categoryItemClick: (category: string) => void;
}): JSX.Element {
  const { categoryList, loading, selCategory, categoryItemClick } = props;
  const classes = useStyles();
  return (
    <List className={classes.list}>
      <ListSubheader color="inherit">Categories</ListSubheader>
      {loading === false ? (
        categoryList.map((category) => (
          <ListItem
            key={category}
            selected={selCategory === category}
            onClick={() => categoryItemClick(category)}
            button
          >
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                <LocalActivitySharpIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={category} secondary={null} />
          </ListItem>
        ))
      ) : (
        <Skeleton />
      )}
    </List>
  );
}

export default CategoryList;
