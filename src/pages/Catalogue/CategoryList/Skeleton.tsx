import React from 'react';
import CustomSkeleton from '../../../components/CustomSkeleton';

function Skeleton(): JSX.Element {
  const skeletonList = [1, 2, 3, 4, 5];
  return (
    <>
      {skeletonList.map((i) => (
        <CustomSkeleton key={i} type="categoryItem" />
      ))}
    </>
  );
}

export default Skeleton;
