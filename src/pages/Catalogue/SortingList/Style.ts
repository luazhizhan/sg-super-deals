import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  radioBtn: {
    color: theme.palette.primary.dark,
  },
}));

export default useStyles;
