import React from 'react';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import SortFilterCard from '../../../components/SortFilterCard';
import useStyles from './Style';

function SortingList(props: {
  sortBy: string;
  sortingRadioBtnChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}): JSX.Element {
  const { sortBy, sortingRadioBtnChange } = props;
  const classes = useStyles();

  return (
    <SortFilterCard title="Sort by">
      <RadioGroup
        aria-label="sort by"
        name="sort"
        value={sortBy}
        onChange={(event) => sortingRadioBtnChange(event)}
      >
        <FormControlLabel
          value="descDate"
          control={<Radio color="primary" className={classes.radioBtn} />}
          label="Date: Descending"
        />
        <FormControlLabel
          value="ascnDate"
          control={<Radio color="primary" className={classes.radioBtn} />}
          label="Date: Ascending"
        />
      </RadioGroup>
    </SortFilterCard>
  );
}

export default SortingList;
