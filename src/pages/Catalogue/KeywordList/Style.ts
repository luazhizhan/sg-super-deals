import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  card: {
    margin: '0.5em 0',
    overflowY: 'auto',
  },
  title: {
    marginBottom: '1em',
  },
}));

export default useStyles;
