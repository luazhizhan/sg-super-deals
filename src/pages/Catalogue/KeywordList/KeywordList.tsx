import React from 'react';
import Box from '@material-ui/core/Box';
import Skeleton from './Skeleton';
import KeywordChip from '../../../components/KeywordChip';
import SortFilterCard from '../../../components/SortFilterCard';

function KeywordList(props: {
  keywordList: string[];
  loading: boolean;
  selKeyword: string;
  keywordClick: (keyword: string) => void;
}): JSX.Element {
  const { keywordList, loading, selKeyword, keywordClick } = props;

  return (
    <SortFilterCard title="Keywords">
      <Box display="flex" flexWrap="wrap" alignContent="space-around">
        {loading === false ? (
          keywordList.map((keyword) => (
            <KeywordChip
              key={keyword}
              keyword={keyword}
              keywordClick={keywordClick}
              variant={selKeyword === keyword ? 'default' : 'outlined'}
            />
          ))
        ) : (
          <Skeleton />
        )}
      </Box>
    </SortFilterCard>
  );
}

export default KeywordList;
