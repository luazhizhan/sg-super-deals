import React from 'react';
import Grid from '@material-ui/core/Grid';
import CustomSkeleton from '../../../components/CustomSkeleton';

function Skeleton(): JSX.Element {
  const skeletonList = [1, 2, 3, 4, 5, 6, 7, 8];
  return (
    <>
      {skeletonList.map((i) => (
        <Grid item key={i} xs={12} sm={6} md={6} lg={4} xl={3}>
          <CustomSkeleton type="promoCard" />
        </Grid>
      ))}
    </>
  );
}

export default Skeleton;
