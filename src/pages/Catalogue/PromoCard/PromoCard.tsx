import React from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import useStyles from './Style';
import PromoDesc from '../../../components/PromoDesc';
import PromoCardActions from '../../../components/PromoCardActions';
import Skeleton from './Skeleton';
import Promotion from '../../../interfaces/data/Promotion';

function PromoCard(props: { data: Promotion[]; loading: boolean }): JSX.Element {
  const { data, loading } = props;
  const classes = useStyles();
  return (
    <Grid container spacing={2} className={classes.container}>
      {loading === false ? (
        data.map((promo) => (
          <Grid key={promo.promotionID} item xs={12} sm={6} md={6} lg={4} xl={3}>
            <Card className={classes.card}>
              <CardMedia image={promo.imageUrl} className={classes.cardMedia} />
              <CardContent className={classes.cardContent}>
                <Typography variant="h6" gutterBottom>
                  {promo.title}
                </Typography>
                <PromoDesc promo={promo} />
              </CardContent>
              <PromoCardActions promo={promo} />
            </Card>
          </Grid>
        ))
      ) : (
        <Skeleton />
      )}
    </Grid>
  );
}

export default PromoCard;
