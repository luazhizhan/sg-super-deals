import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  card: {
    maxHeight: '100%',
  },
  container: {
    margin: '1.5em 0 !important',
    paddingRight: '1em',
  },
  cardMedia: {
    minHeight: '18em',
    backgroundSize: 'contain !important',
  },
  cardContent: {
    minHeight: '22em',
    maxHeight: '100%',
  },
}));

export default useStyles;
