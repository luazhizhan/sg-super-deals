import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  searchPaper: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '99%',
    margin: '1.5em auto',
  },
  searchInput: {
    flex: 1,
    marginLeft: theme.spacing(1),
  },
  searchIcon: {
    padding: 10,
  },
  formDivider: {
    height: 0,
    margin: 4,
  },
  btnGroup: {
    display: 'initial',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
}));

export default useStyles;
