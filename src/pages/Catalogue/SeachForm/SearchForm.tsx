import React from 'react';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import ViewListIcon from '@material-ui/icons/ViewList';
import ViewColumnIcon from '@material-ui/icons/ViewColumn';
import useStyles from './Style';
import ViewTypesBtnGrp from '../../../interfaces/pages/ViewTypesBtnGrp';
import TitleTxt from '../../../components/TitleTxt';

function SearchFrom(props: {
  variants: ViewTypesBtnGrp;
  viewClick: (txt: string) => void;
  searchVal: string;
  handleSearchInputChange: (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => void;
  handleClearSearchClick: () => void;
}): JSX.Element {
  const { variants, viewClick, searchVal, handleSearchInputChange, handleClearSearchClick } = props;
  const classes = useStyles();
  return (
    <>
      <TitleTxt id="searchPromo" title="Search for deals and promotions" />
      <Paper component="form" className={classes.searchPaper}>
        <InputBase
          className={classes.searchInput}
          onChange={(event) => handleSearchInputChange(event)}
          placeholder="Gong cha"
          value={searchVal}
        />
        <Divider className={classes.formDivider} orientation="vertical" />
        <IconButton className={classes.searchIcon} aria-label="search">
          <SearchIcon />
        </IconButton>
      </Paper>
      <div hidden={searchVal === ''} style={{ clear: 'both', marginBottom: '1.5em' }}>
        <Button
          onClick={handleClearSearchClick}
          size="small"
          variant="contained"
          color="primary"
          startIcon={<ClearIcon />}
        >
          Clear Search
        </Button>
      </div>
      <ButtonGroup color="primary" className={classes.btnGroup}>
        <Button variant={variants.col} onClick={() => viewClick('col')}>
          <ViewColumnIcon />
        </Button>
        <Button variant={variants.list} onClick={() => viewClick('list')}>
          <ViewListIcon />
        </Button>
      </ButtonGroup>
    </>
  );
}

export default SearchFrom;
