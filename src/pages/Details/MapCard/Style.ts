import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  chip: {
    marginRight: 10,
    marginBottom: 15,
  },
}));

export default useStyles;
