import React from 'react';
import Card from '@material-ui/core/Card';
import Chip from '@material-ui/core/Chip';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import useDetailsStyles from '../Style';
import DetailsCardContent from '../DetailsCardContent/DetailsCardContent';
import Promotion from '../../../interfaces/data/Promotion';
import { GooleMapEmbedURL } from '../../../configs/UrlConfigs';
import useStyles from './Style';

// TODO: ADD SELECTED LOCATION FUNCTION
function MapCard(props: {
  promo: Promotion;
  selLoc: string;
  locationClick: (location: string) => void;
}) {
  const classes = useStyles();
  const { promo, selLoc, locationClick } = props;
  const detailsClasses = useDetailsStyles();
  return (
    <>
      {promo.locations ? (
        <Card className={detailsClasses.card}>
          <DetailsCardContent title="Location">
            {promo.locations.map((loc) => (
              <Chip
                icon={<LocationOnIcon />}
                variant={selLoc === loc ? 'default' : 'outlined'}
                key={loc}
                label={loc}
                color="primary"
                onClick={() => locationClick(loc)}
                className={classes.chip}
                clickable
              />
            ))}
            <iframe
              title={selLoc}
              height="300"
              frameBorder="0"
              style={{ border: 0, width: '100%' }}
              src={GooleMapEmbedURL(selLoc)}
              allowFullScreen
            />
          </DetailsCardContent>
        </Card>
      ) : null}
    </>
  );
}

export default MapCard;
