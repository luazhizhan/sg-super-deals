import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  div: {
    margin: '0 auto',
    width: '98%',
  },
  card: {
    margin: '1.5em auto',
  },
  img: {
    display: 'block',
    margin: '0 auto',
    width: '30%',
    [theme.breakpoints.down('md')]: {
      width: '40%',
    },
    [theme.breakpoints.down('xs')]: {
      width: '80%',
    },
  },
  notFoundDiv: {
    marginTop: '8em',
    textAlign: 'center',
  },
  btn: {
    marginBottom: '1em',
  },
  skeleton: {
    width: '98%',
    margin: '1.5em auto',
  },
}));

export default useStyles;
