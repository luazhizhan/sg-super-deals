import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton/Skeleton';
import useStyles from '../Style';

function LoadingPanel() {
  const classes = useStyles();
  return (
    <div className={classes.div}>
      <Skeleton variant="rect" height={250} className={classes.img} />
      <Skeleton variant="rect" height={200} className={classes.skeleton} />
      <Skeleton variant="rect" height={200} className={classes.skeleton} />
      <Skeleton variant="rect" height={200} className={classes.skeleton} />
    </div>
  );
}

export default LoadingPanel;
