import React from 'react';
import Card from '@material-ui/core/Card';
import DetailsCardContent from '../DetailsCardContent/DetailsCardContent';
import useStyles from '../Style';
import KeywordChip from '../../../components/KeywordChip';
import Promotion from '../../../interfaces/data/Promotion';

function KeywordsCard(props: { promo: Promotion; keywordClick: (keyword: string) => void }) {
  const { promo, keywordClick } = props;
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <DetailsCardContent title="Keywords">
        {promo.keywords.map((keyword) => (
          <KeywordChip
            key={keyword}
            keyword={keyword}
            keywordClick={keywordClick}
            variant="outlined"
          />
        ))}
      </DetailsCardContent>
    </Card>
  );
}

export default KeywordsCard;
