import React from 'react';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import DetailsCardContent from '../DetailsCardContent/DetailsCardContent';

import useStyles from '../Style';
import btnUseStyles from '../../../assets/styles/BtnStyle';
import Promotion from '../../../interfaces/data/Promotion';

function SrcCard(props: { promo: Promotion }) {
  const { promo } = props;
  const classes = useStyles();
  const btnClasses = btnUseStyles();

  return (
    <>
      {promo.displaySrc ? (
        <Card className={classes.card}>
          <DetailsCardContent title="Source Information">
            <Typography variant="body1">
              Source: {promo.sourceCategory}
              <br />
              Name: {promo.srcName}
            </Typography>
          </DetailsCardContent>
          <CardActions>
            <Button
              href={promo.srcUrl}
              target="_blank"
              rel="noopener"
              color="primary"
              className={btnClasses.noWrapbtn}
            >
              Learn More
            </Button>
          </CardActions>
        </Card>
      ) : null}
    </>
  );
}

export default SrcCard;
