import React from 'react';
import Button from '@material-ui/core/Button';
import AppsIcon from '@material-ui/icons/Apps';
import EmptyList from '../../../components/EmptyList';
import detailsNotFound from '../../../assets/images/details-not-found.svg';
import useStyles from '../Style';

function NotFoundPanel() {
  const classes = useStyles();
  return (
    <div className={classes.notFoundDiv}>
      <EmptyList
        img={detailsNotFound}
        title="Promotion not found"
        body="The promotion may have expired"
      />
      <Button
        href="/catalogue"
        color="primary"
        variant="contained"
        size="large"
        startIcon={<AppsIcon />}
        className={classes.btn}
      >
        Catalogue
      </Button>
    </div>
  );
}

export default NotFoundPanel;
