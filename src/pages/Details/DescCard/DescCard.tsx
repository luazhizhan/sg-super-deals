import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import DetailsCardContent from '../DetailsCardContent/DetailsCardContent';
import useStyles from '../Style';
import btnUseStyles from '../../../assets/styles/BtnStyle';

import Promotion from '../../../interfaces/data/Promotion';
import PromoDesc from '../../../components/PromoDesc';
import CopyBtn from '../../../components/CopyBtn';

function DescCard(props: { promo: Promotion; handleCopyCodeClick: () => void }) {
  const { promo, handleCopyCodeClick } = props;
  const classes = useStyles();
  const btnClasses = btnUseStyles();

  return (
    <Card className={classes.card}>
      <DetailsCardContent title={promo.title}>
        <PromoDesc promo={promo} />
      </DetailsCardContent>
      <CardActions>
        {promo.url ? (
          <Button
            href={promo.url}
            target="_blank"
            rel="noopener"
            color="primary"
            className={btnClasses.noWrapbtn}
          >
            Learn More
          </Button>
        ) : null}
        {promo.code ? (
          <CopyBtn value={promo.code}>
            <Button
              color="primary"
              onClick={() => handleCopyCodeClick()}
              className={btnClasses.noWrapbtn}
            >
              Copy code to clipboard
            </Button>
          </CopyBtn>
        ) : null}
      </CardActions>
    </Card>
  );
}

export default DescCard;
