import React, { ReactNode } from 'react';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

function DetailsCardContent(props: { title: string; children: ReactNode }): JSX.Element {
  const { title, children } = props;
  return (
    <>
      <CardContent>
        <Typography variant="h6" gutterBottom>
          {title}
        </Typography>
        {children}
      </CardContent>
    </>
  );
}

export default DetailsCardContent;
