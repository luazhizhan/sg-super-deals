import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  grid: {
    backgroundColor: 'white',
    margin: '2em auto',
    padding: '1em 0',
    width: '99%',
    boxShadow: theme.shadows[1],
  },
  image: {
    height: 250,
    [theme.breakpoints.down('xs')]: {
      height: 180,
    },
    display: 'block',
    margin: '0 auto',
  },
  introBtnGrid: {
    margin: '1em auto',
  },
  containedBtn: {
    marginTop: '2em',
    marginRight: '1em',
  },
  outlinedBtn: {
    marginTop: '2em',
  },
}));

export default useStyles;
