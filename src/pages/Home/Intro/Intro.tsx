import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import HomeImg from '../../../assets/images/intro.svg';
import useStyles from './Style';

function Intro(): JSX.Element {
  const classes = useStyles();
  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid xs={12} sm={12} md={6} item>
        <img src={HomeImg} alt="Logo" className={classes.image} />
      </Grid>
      <Grid xs={12} sm={12} md={6} className={classes.introBtnGrid} item>
        <Typography variant="h4">
          Find deals and promotions <br />
          in Singapore here
        </Typography>
        <Button
          href="/catalogue"
          variant="contained"
          color="primary"
          className={classes.containedBtn}
        >
          View Catalogue
        </Button>
        <Button
          href="#latestPromo"
          variant="outlined"
          color="primary"
          className={classes.outlinedBtn}
        >
          Latest Deals
        </Button>
      </Grid>
    </Grid>
  );
}

export default Intro;
