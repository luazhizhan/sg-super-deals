import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  card: {
    margin: '1.5em auto',
  },
  body: {
    display: 'flex',
    marginTop: '15px',
  },
  icon: {
    margin: '-6px 8px 0 0',
  },
}));

export default useStyles;
