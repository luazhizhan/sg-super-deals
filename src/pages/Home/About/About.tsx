import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import InfoIcon from '@material-ui/icons/Info';
import ImageIcon from '@material-ui/icons/Image';
import TitleTxt from '../../../components/TitleTxt';
import useStyles from './Style';

function About() {
  const classes = useStyles();
  return (
    <>
      <TitleTxt id="aboutUs" title="About Us" />
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="body1" className={classes.body}>
            <InfoIcon fontSize="large" color="primary" className={classes.icon} />
            SG Super Deals provides deals and promotions in Singapore for free.
          </Typography>
          <Typography variant="body1" className={classes.body}>
            <ImageIcon fontSize="large" color="primary" className={classes.icon} />
            Illustrations by&nbsp;{' '}
            <a href="https://undraw.co/" target="_blank" rel="noopener noreferrer">
              undraw
            </a>
          </Typography>
        </CardContent>
      </Card>
    </>
  );
}

export default About;
