import React from 'react';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TitleTxt from '../../../components/TitleTxt';
import useStyles from './Style';
import dessertDrink from '../../../assets/images/dessert-drink.svg';
import fastFood from '../../../assets/images/fast-food.svg';
import foodDelivery from '../../../assets/images/food-delivery.svg';

function DealPromo() {
  const classes = useStyles();
  return (
    <>
      <TitleTxt id="promoDealsCat" title="Promotion and Deal Categories" />
      <Card className={classes.card}>
        <CardContent>
          <Grid container spacing={2} className={classes.grid}>
            <Grid xs={12} sm={4} component={Link} to="/catalogue" className={classes.gridItem} item>
              <img src={foodDelivery} alt="Food Delivery" className={classes.img} />
              <Typography variant="h6" className={classes.label}>
                Food Delivery
              </Typography>
            </Grid>
            <Grid xs={12} sm={4} component={Link} to="/catalogue" className={classes.gridItem} item>
              <img src={fastFood} alt="Fast Food" className={classes.img} />
              <Typography variant="h6" className={classes.label}>
                Fast Food
              </Typography>
            </Grid>
            <Grid xs={12} sm={4} component={Link} to="/catalogue" className={classes.gridItem} item>
              <img src={dessertDrink} alt="Dessert and Drink" className={classes.img} />
              <Typography variant="h6" className={classes.label}>
                Dessert and Drink
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </>
  );
}

export default DealPromo;
