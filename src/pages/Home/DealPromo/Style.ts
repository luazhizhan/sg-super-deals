import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  card: {
    margin: '1.5em auto',
  },
  grid: {
    textAlign: 'center',
  },
  gridItem: {
    cursor: 'pointer',
    textDecoration: 'none',
    color: 'black',
    '&:hover': {
      backgroundColor: 'rgba(233, 30, 99, 0.08)',
    },
  },
  label: {
    fontWeight: 400,
  },
  img: {
    height: 160,
    [theme.breakpoints.down('sm')]: {
      height: 130,
    },
  },
}));

export default useStyles;
