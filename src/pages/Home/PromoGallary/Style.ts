import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  box: {
    marginBottom: '1em',
  },
  firstThreePromo: {
    width: '32.3%',
    margin: '1em 0.5% 1em 0.5%',
    cursor: 'pointer',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[1],
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  lastTwoPromo: {
    width: '49%',
    margin: '0 0.5% 1em 0.5%',
    cursor: 'pointer',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[1],
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  promoImg: {
    objectFit: 'contain',
    width: '100%',
  },
  promoTitle: {
    fontSize: '1.3em',
    height: '2.2em',
    paddingTop: '0.2em',
    paddingLeft: '0.5em',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    fontWeight: 450,
  },
}));

export default useStyles;
