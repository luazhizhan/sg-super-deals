/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Skeleton from './Skeleton';
import useStyles from './Style';
import Promotion from '../../../interfaces/data/Promotion';
import TitleTxt from '../../../components/TitleTxt';

function PromoGallery(props: {
  data: Promotion[];
  gallaryClick: (promoId: string) => void;
  loading: boolean;
}): JSX.Element {
  const classes = useStyles();
  const { data, gallaryClick, loading } = props;
  return (
    <>
      <TitleTxt id="latestPromo" title="Latest Promotions and Deals" />
      <Box display="flex" flexWrap="wrap" alignContent="space-around" className={classes.box}>
        {loading === false ? (
          data.map((promo, index) => (
            <div
              onClick={() => gallaryClick(promo.promotionID)}
              key={promo.promotionID}
              className={index <= 2 ? classes.firstThreePromo : classes.lastTwoPromo}
            >
              <Typography
                className={classes.promoTitle}
                variant="subtitle1"
                align="left"
                gutterBottom
              >
                {promo.title}
              </Typography>
              <img
                className={classes.promoImg}
                height="200"
                src={promo.imageUrl}
                alt={promo.title}
              />
            </div>
          ))
        ) : (
          <Skeleton />
        )}
      </Box>
    </>
  );
}

export default PromoGallery;
