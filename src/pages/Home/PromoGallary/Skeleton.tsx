import React from 'react';
import CustomSkeleton from '../../../components/CustomSkeleton';

function Skeleton(): JSX.Element {
  return (
    <>
      <CustomSkeleton type="promoGallarySmall" />
      <CustomSkeleton type="promoGallarySmall" />
      <CustomSkeleton type="promoGallarySmall" />
      <CustomSkeleton type="promoGallaryBig" />
      <CustomSkeleton type="promoGallaryBig" />
    </>
  );
}

export default Skeleton;
