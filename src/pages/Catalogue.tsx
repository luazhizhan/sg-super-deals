import React, { useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import {
  setViewVariants,
  allPromoDataRequest,
  sortRequest,
  categoryRequest,
  keywordRequest,
  searchRequest,
  clearSearch,
} from '../redux/Catalogue/actions';
import { showSnackbar, drawerItemSelUpdate } from '../redux/Root/actions';
import { scrollToAnchor } from '../helpers/Common';
import ViewTypesBtnGrp from '../interfaces/pages/ViewTypesBtnGrp';
import CatalogueState from '../interfaces/redux/Catalogue';
import SearchForm from './Catalogue/SeachForm/SearchForm';
import PromoCard from './Catalogue/PromoCard/PromoCard';
import PromoList from './Catalogue/PromoList/PromoList';
import CategoryList from './Catalogue/CategoryList/CategoryList';
import KeywordList from './Catalogue/KeywordList/KeywordList';
import SortingList from './Catalogue/SortingList/SortingList';
import HiddenAnchor from '../components/HiddenAnchor';
import EmptyList from '../components/EmptyList';
import EmptyPromo from '../assets/images/empty-promo.svg';

function Catalogue() {
  const dispatch = useDispatch();
  const promotionState = useSelector((state: { Catalogue: CatalogueState }) => state.Catalogue);
  const {
    variants,
    sortBy,
    promo,
    keywords,
    categories,
    search,
    loading,
    isError,
    error,
  } = promotionState;

  const viewClick = (txt: string) => {
    const viewTypesBtns: ViewTypesBtnGrp =
      txt === 'col'
        ? { col: 'contained', list: 'outlined' }
        : { col: 'outlined', list: 'contained' };
    dispatch(setViewVariants({ variants: viewTypesBtns }));
  };

  const sortingRadioBtnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    scrollToAnchor('catalogue');
    dispatch(sortRequest({ sortBy: e.target.value }));
  };

  const categoryItemClick = (category: string) => {
    scrollToAnchor('catalogue');
    dispatch(categoryRequest({ category }));
  };

  const keywordClick = (keyword: string) => {
    scrollToAnchor('catalogue');
    dispatch(keywordRequest({ keyword }));
  };

  const handleSearchInputChange = (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => {
    dispatch(searchRequest({ search: event.target.value }));
  };

  const handleClearSearchClick = () => {
    dispatch(clearSearch());
  };

  const fetchData = useCallback(async () => {
    dispatch(drawerItemSelUpdate({ sel: 'catalogue' }));
    dispatch(allPromoDataRequest());
  }, [dispatch]);

  useEffect(() => {
    window.scrollTo(0, 0);
    if (isError) {
      dispatch(showSnackbar({ msg: error, variant: 'error' }));
    } else {
      fetchData();
    }
  }, [dispatch, error, fetchData, isError]);
  return (
    <>
      <Grid container spacing={2}>
        <Grid xs={12} item>
          <SearchForm
            searchVal={search}
            variants={variants}
            viewClick={viewClick}
            handleSearchInputChange={handleSearchInputChange}
            handleClearSearchClick={handleClearSearchClick}
          />
        </Grid>
        <Grid xs={12} sm={12} md={3} item>
          <SortingList sortBy={sortBy} sortingRadioBtnChange={sortingRadioBtnChange} />
          <CategoryList
            categoryList={categories.data}
            selCategory={categories.selected}
            loading={loading}
            categoryItemClick={categoryItemClick}
          />
          <KeywordList
            keywordList={keywords.data}
            loading={loading}
            selKeyword={keywords.selected}
            keywordClick={keywordClick}
          />
        </Grid>
        <Grid xs={12} sm={12} md={9} item>
          <HiddenAnchor id="catalogue" title="Promotion Catalogue" />
          {variants.col === 'contained' ? (
            <PromoCard data={promo.viewData} loading={loading} />
          ) : (
            <PromoList data={promo.viewData} />
          )}
          {promo.viewData.length ? null : (
            <EmptyList
              img={EmptyPromo}
              title="No Promotions found"
              body="Try seaching for other promotions instead"
            />
          )}
        </Grid>
      </Grid>
    </>
  );
}

export default Catalogue;
