import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: '#272c34',
    color: theme.palette.text.secondary,
    textAlign: 'center',
    height: '6em',
    width: '100%',
    position: 'relative',
    bottom: 0,
  },
  footerTxt1: {
    paddingTop: '1em',
  },
  footerTxt2: {
    paddingTop: '5px',
    fontSize: '13px',
    color: '#bdbdbd',
  },
}));

export default useStyles;
