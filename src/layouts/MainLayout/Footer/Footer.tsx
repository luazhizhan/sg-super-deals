import React from 'react';
import Typography from '@material-ui/core/Typography';
import useStyles from './Style';

function Footer(): JSX.Element {
  const classes = useStyles();
  return (
    <div className={classes.footer}>
      <Typography variant="body1" className={classes.footerTxt1}>
        Singapore deals and promotions
      </Typography>
      <Typography variant="body2" className={classes.footerTxt2}>
        Copyright © SG Super Deals {new Date().getFullYear()}
      </Typography>
    </div>
  );
}

export default Footer;
