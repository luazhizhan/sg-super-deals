import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { Link } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import logo from '../../../assets/images/logo.png';
import useStyles from './Style';

function Header(props: { handleDrawerOpen: () => void }): JSX.Element {
  const { handleDrawerOpen } = props;
  const classes = useStyles();
  return (
    <AppBar position="fixed">
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleDrawerOpen}
          className={classes.iconBtn}
          edge="start"
        >
          <MenuIcon />
        </IconButton>
        <Link href="/">
          <img src={logo} alt="logo" height="50" width="50" className={classes.logo} />
        </Link>
        <Typography variant="h6" className={classes.title}>
          SG Super Deals
        </Typography>
        <div className={classes.menuBtns}>
          <Button color="inherit" href="/">
            Home
          </Button>
          <Button color="inherit" href="/catalogue">
            Catalogue
          </Button>
        </div>
      </Toolbar>
    </AppBar>
  );
}

export default Header;
