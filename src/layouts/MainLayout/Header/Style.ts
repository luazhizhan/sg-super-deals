import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  logo: {
    borderRadius: '10%',
    marginRight: '10px',
    marginTop: '10px',
  },
  title: {
    flexGrow: 1,
  },
  iconBtn: {
    display: 'none',
    [theme.breakpoints.down('xs')]: {
      display: 'initial',
    },
  },
  menuBtns: {
    display: 'initial',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
}));

export default useStyles;
