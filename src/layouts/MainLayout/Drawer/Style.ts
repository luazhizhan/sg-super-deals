import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: '240px',
  },
  drawerList: {
    paddingTop: 0,
  },
  drawerHeader: {
    background: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    height: 66,
  },
  drawerTitle: {
    marginTop: 18,
    marginLeft: 10,
  },
}));

export default useStyles;
