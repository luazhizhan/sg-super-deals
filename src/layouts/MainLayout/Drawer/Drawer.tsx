import React from 'react';
import { useSelector } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import ListDrawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import AppsIcon from '@material-ui/icons/Apps';
import RootState from '../../../interfaces/redux/Root';
import useStyles from './Style';

function Drawer(props: {
  handleDrawerClose: () => void;
  menuItemClick: (page: string) => void;
}): JSX.Element {
  const { handleDrawerClose, menuItemClick } = props;
  const rootState = useSelector((state: { Root: RootState }) => state.Root);
  const { drawer } = rootState;
  const classes = useStyles();

  return (
    <ListDrawer
      classes={{
        paper: classes.drawer,
      }}
      open={drawer.open}
      onClose={handleDrawerClose}
    >
      <header className={classes.drawerHeader}>
        <Typography variant="h6" className={classes.drawerTitle}>
          SG Super Deals
        </Typography>
      </header>
      <List component="nav" className={classes.drawerList} aria-labelledby="nested-list-subheader">
        <ListItem selected={drawer.sel === 'home'} onClick={() => menuItemClick('home')} button>
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
          <ListItemText primary="Home" />
        </ListItem>
        <ListItem
          selected={drawer.sel === 'catalogue'}
          onClick={() => menuItemClick('catalogue')}
          button
        >
          <ListItemIcon>
            <AppsIcon />
          </ListItemIcon>
          <ListItemText primary="Catalogue" />
        </ListItem>
      </List>
    </ListDrawer>
  );
}

export default Drawer;
