import React, { ReactNode } from 'react';
import { push } from 'connected-react-router';
import { useDispatch } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider, makeStyles } from '@material-ui/core/styles';
import { drawerOpen, drawerClose, drawerItemSelUpdate } from '../redux/Root/actions';
import Header from './MainLayout/Header/Header';
import Drawer from './MainLayout/Drawer/Drawer';
import MainTheme from '../assets/themes/MainTheme';
import Footer from './MainLayout/Footer/Footer';
import CustomSnackbar from '../components/Snackbar/CustomSnackbar';

const useStyles = makeStyles(() => ({
  main: {
    margin: '6em 1.5em 2em 1.5em',
    minHeight: '100vh',
  },
}));

function MainLayout(props: { children: ReactNode }): JSX.Element {
  const { children } = props;
  const dispatch = useDispatch();
  const classes = useStyles();

  const handleDrawerOpen = () => {
    dispatch(drawerOpen());
  };

  const handleDrawerClose = () => {
    dispatch(drawerClose());
  };

  const menuItemClick = (page: string) => {
    dispatch(drawerItemSelUpdate({ sel: page }));
    if (page === 'home') {
      dispatch(push('/'));
    } else {
      dispatch(push('/catalogue'));
    }
  };

  return (
    <ThemeProvider theme={MainTheme}>
      <CssBaseline />
      <Header handleDrawerOpen={handleDrawerOpen} />
      <Drawer handleDrawerClose={handleDrawerClose} menuItemClick={menuItemClick} />
      <main className={classes.main}>
        {children}
        <CustomSnackbar />
      </main>
      <Footer />
    </ThemeProvider>
  );
}

export default MainLayout;
