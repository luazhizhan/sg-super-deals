import React from 'react';
import { Link } from 'react-router-dom';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import btnUseStyles from '../assets/styles/BtnStyle';
import Promotion from '../interfaces/data/Promotion';

function PromoCardActions(props: { promo: Promotion }): JSX.Element {
  const classes = btnUseStyles();
  const { promo } = props;
  return (
    <CardActions>
      <Button
        component={Link}
        to={`/details/${promo.promotionID}`}
        color="primary"
        className={classes.noWrapbtn}
      >
        Details
      </Button>
      {promo.url ? (
        <Button
          href={promo.url}
          target="_blank"
          rel="noopener"
          color="primary"
          className={classes.noWrapbtn}
        >
          Learn More
        </Button>
      ) : null}
    </CardActions>
  );
}

export default PromoCardActions;
