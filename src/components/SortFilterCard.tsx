import React, { ReactNode } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  card: {
    margin: '2em 0',
    overflowY: 'auto',
  },
  title: {
    marginBottom: '1em',
  },
}));

function SortFilterCard(props: { title: string; children: ReactNode }): JSX.Element {
  const { title, children } = props;
  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title} variant="subtitle2" gutterBottom>
          {title}
        </Typography>
        {children}
      </CardContent>
    </Card>
  );
}

export default SortFilterCard;
