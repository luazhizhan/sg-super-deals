import React, { ReactNode } from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';

function CopyBtn(props: { value: string; children: ReactNode }) {
  const { value, children } = props;
  return <CopyToClipboard text={value}>{children}</CopyToClipboard>;
}

export default CopyBtn;
