import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  promoGallarySmall: {
    width: '32.3%',
    height: 220,
    margin: '1em 0.5% 1em 0.5%',
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  promoGallaryBig: {
    width: '49%',
    height: 220,
    margin: '0 0.5% 1em 0.5%',
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  promoCard: {
    width: '100%',
    height: 350,
  },
  keywordChip: {
    width: 100,
    height: 20,
    borderRadius: '1em',
    margin: '0.5em 1%',
  },
  categoryItem: {
    height: 50,
    width: '96%',
    margin: '0.5em 2%',
  },
}));

function CustomSkeleton(props: {
  type: 'promoGallarySmall' | 'promoGallaryBig' | 'promoCard' | 'keywordChip' | 'categoryItem';
}): JSX.Element {
  const { type } = props;
  const classes = useStyles();
  let style: string;
  switch (type) {
    case 'promoGallarySmall':
      style = classes.promoGallarySmall;
      break;
    case 'promoGallaryBig':
      style = classes.promoGallaryBig;
      break;
    case 'promoCard':
      style = classes.promoCard;
      break;
    case 'keywordChip':
      style = classes.keywordChip;
      break;
    case 'categoryItem':
      style = classes.categoryItem;
      break;
    default:
      style = classes.promoGallarySmall;
      break;
  }

  return <Skeleton variant="rect" className={style} />;
}

export default CustomSkeleton;
