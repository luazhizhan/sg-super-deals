import React, { ReactNode } from 'react';
import { withStyles } from '@material-ui/core/styles';

interface Preformatted {
  root: {
    overflowWrap: 'break-word';
    whiteSpace: 'pre-wrap';
  };
}

const styles: Preformatted = {
  root: {
    overflowWrap: 'break-word',
    whiteSpace: 'pre-wrap',
  },
};

function PreformattedTxt(props: { classes: { root: string }; children: ReactNode }): JSX.Element {
  const { classes, children } = props;
  return <pre className={classes.root}>{children}</pre>;
}

export default withStyles(styles)(PreformattedTxt);
