import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  title: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    width: 'fit-content',
    paddingBottom: '0.5em',
    marginLeft: '0.3em !important',
  },
}));

function TitleTxt(props: { id: string; title: string }): JSX.Element {
  const { id, title } = props;
  const classes = useStyles();
  return (
    <Typography id={id} variant="h5" className={classes.title} gutterBottom>
      {title}
    </Typography>
  );
}

export default TitleTxt;
