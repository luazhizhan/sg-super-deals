import React from 'react';
import Chip from '@material-ui/core/Chip';
import LocalActivitySharpIcon from '@material-ui/icons/LocalActivitySharp';
import { styled } from '@material-ui/core/styles';

const KeywordChipStyled = styled(Chip)({
  margin: '0.5em 0.5em',
});

function KeywordChip(props: {
  keyword: string;
  variant: 'default' | 'outlined';
  keywordClick: (keyword: string) => void;
}): JSX.Element {
  const { keyword, keywordClick, variant } = props;

  return (
    <KeywordChipStyled
      icon={<LocalActivitySharpIcon />}
      variant={variant}
      label={keyword}
      color="primary"
      onClick={() => keywordClick(keyword)}
      clickable
    />
  );
}

export default KeywordChip;
