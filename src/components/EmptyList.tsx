import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  div: {
    textAlign: 'center',
    marginBottom: '3.5em',
  },
  title: {
    margin: '0.5em auto',
  },
  image: {
    height: 250,
    [theme.breakpoints.down('xs')]: {
      height: 200,
    },
  },
}));

function EmptyList(props: { title: string; body: string; img: string }): JSX.Element {
  const { title, body, img } = props;
  const classes = useStyles();
  return (
    <div className={classes.div}>
      <img src={img} alt={title} className={classes.image} />
      <Typography variant="h5" className={classes.title}>
        {title}
      </Typography>
      <Typography variant="body1">{body}</Typography>
    </div>
  );
}

export default EmptyList;
