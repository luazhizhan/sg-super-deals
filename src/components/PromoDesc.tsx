import React from 'react';
import Moment from 'react-moment';
import Typography from '@material-ui/core/Typography';
import PreformattedTxt from './PreformattedTxt';
import Promotion from '../interfaces/data/Promotion';

function PromoDesc(props: { promo: Promotion }): JSX.Element {
  const { promo } = props;
  return (
    <PreformattedTxt>
      <Typography variant="body2">
        <span role="img" aria-label="category">
          🏷️
        </span>
        Category: {promo.category}
        <br />
        {`✅ ${promo.desc}`}
        {promo.dueDate ? (
          <>
            <br />
            📅 Due Date: <Moment format="DD MMM YYYY">{promo.dueDate}</Moment>
          </>
        ) : undefined}
        {promo.condition ? (
          <>
            <br />
            🔴 {promo.condition}
          </>
        ) : undefined}
      </Typography>
    </PreformattedTxt>
  );
}

export default PromoDesc;
