import React from 'react';

function HiddenAnchor(props: { id: string; title: string }): JSX.Element {
  const { id, title } = props;
  return (
    <h2 id={id} style={{ visibility: 'hidden', height: 0, margin: 0 }}>
      {title}
    </h2>
  );
}

export default HiddenAnchor;
