import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarWrapper from './SnackbarWrapper';
import { hideSnackbar } from '../../redux/Root/actions';
import RootState from '../../interfaces/redux/Root';

function CustomSnackbar(): JSX.Element {
  const dispatch = useDispatch();
  const rootState = useSelector((state: { Root: RootState }) => state.Root);
  const { snackbar } = rootState;

  const handleClose = (reason: string) => {
    if (reason === 'clickaway') {
      return;
    }
    dispatch(hideSnackbar());
  };

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      open={snackbar.open}
      autoHideDuration={3000}
      onClose={(_event, reason) => handleClose(reason)}
    >
      <SnackbarWrapper variant={snackbar.variant} message={snackbar.msg} />
    </Snackbar>
  );
}

export default CustomSnackbar;
