import React from 'react';
import clsx from 'clsx';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import WarningIcon from '@material-ui/icons/Warning';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import { SvgIconProps } from '@material-ui/core';
import useStyles from './Style';

const VariantIcon = (variant: string): ((props: SvgIconProps) => JSX.Element) => {
  switch (variant) {
    case 'success':
      return CheckCircleIcon;
    case 'warning':
      return WarningIcon;
    case 'error':
      return ErrorIcon;
    case 'info':
      return InfoIcon;
    case 'copy':
      return FileCopyIcon;
    default:
      return InfoIcon;
  }
};

const SnackBarClassName = (variant: string): string => {
  const classes = useStyles();
  switch (variant) {
    case 'success':
      return classes.success;
    case 'warning':
      return classes.warning;
    case 'error':
      return classes.error;
    case 'info':
      return classes.info;
    case 'copy':
      return classes.copy;
    default:
      return classes.info;
  }
};

function SnackbarWrapper(props: { variant: string; message: string }): JSX.Element {
  const classes = useStyles();
  const { message, variant } = props;
  const Icon = VariantIcon(variant);

  return (
    <SnackbarContent
      className={clsx(SnackBarClassName(variant))}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
    />
  );
}

export default SnackbarWrapper;
