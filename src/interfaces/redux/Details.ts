import Promotion from '../data/Promotion';

interface Home {
  loading: boolean;
  isError: boolean;
  error: string;
  promoId: string;
  selLoc: string;
  promo?: Promotion;
  exists: boolean;
}

export default Home;
