export default interface Root {
  drawer: {
    open: boolean;
    sel: string;
  };
  snackbar: {
    open: boolean;
    msg: string;
    variant: 'success' | 'warning' | 'error' | 'info' | 'copy';
  };
}
