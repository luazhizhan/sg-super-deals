import Promotion from '../data/Promotion';
import ViewTypesBtnGrp from '../pages/ViewTypesBtnGrp';

interface Catalogue {
  loading: boolean;
  isError: boolean;
  error: string;
  variants: ViewTypesBtnGrp;
  promo: {
    selected?: Promotion;
    data: Promotion[];
    viewData: Promotion[];
  };
  keywords: {
    selected: string;
    data: [];
  };
  categories: {
    selected: string;
    data: [];
  };
  search: string;
  sortBy: string;
}

export default Catalogue;
