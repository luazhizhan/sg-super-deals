import Promotion from '../data/Promotion';

interface Home {
  loading: boolean;
  isError: boolean;
  error: string;
  latestPromo: Promotion[];
}

export default Home;
