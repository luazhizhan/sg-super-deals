interface Promotion {
  promotionID: string;
  category: string;
  keywords: string[];
  imageUrl: string;
  title: string;
  desc: string;
  condition?: string;
  code?: string;
  locations?: string[];
  url?: string;
  dueDate?: Date;
  sourceCategory: string;
  displaySrc: boolean;
  srcName: string;
  srcUrl: string;
  creator: string;
  creationDate: Date;
  lastEditor: string;
  editedDate: Date;
}

export default Promotion;
