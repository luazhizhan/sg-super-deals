interface ViewTypesBtnGrp {
  col: 'contained' | 'outlined';
  list: 'contained' | 'outlined';
}

export default ViewTypesBtnGrp;
