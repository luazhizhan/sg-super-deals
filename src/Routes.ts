import Home from './pages/Home';
import Details from './pages/Details';
import Catalogue from './pages/Catalogue';
import NotFound from './pages/NotFound';

const Routes = [
  {
    path: '/',
    id: 'Home',
    name: 'Home',
    component: Home,
  },
  {
    path: '/catalogue',
    id: 'Catalogue',
    name: 'Catalogue',
    component: Catalogue,
  },
  {
    path: '/details/:id',
    id: 'Details',
    name: 'Details',
    component: Details,
  },
  {
    path: '',
    name: 'Not Found',
    component: NotFound,
  },
];

export default Routes;
