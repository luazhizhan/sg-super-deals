import React from 'react';
import { render } from '@testing-library/react';
import App from '../App';

test('renders Home', () => {
  const { getByText } = render(<App />);
  const latestPromoELe = getByText('Latest Promotions and Deals');
  const searchEle = getByText('Search promotion and deals');
  expect(latestPromoELe).toBeInTheDocument();
  expect(searchEle).toBeInTheDocument();
});
