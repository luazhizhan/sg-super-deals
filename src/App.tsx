import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import { store, history } from './redux/store';
import MainLayout from './layouts/MainLayout';
import Routes from './Routes';

function App(): JSX.Element {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <MainLayout>
          <Switch>
            {Routes.map((route) => (
              <Route exact key={route.path} path={route.path} component={route.component} />
            ))}
          </Switch>
        </MainLayout>
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
